<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/auth_key/library/ConstAuthKey.php');
include($strRootPath . '/src/auth_key/boot/AuthKeyBootstrap.php');

include($strRootPath . '/src/requisition/library/ConstRequisition.php');