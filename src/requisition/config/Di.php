<?php

use people_sdk\space_auth_key\requisition\request\info\factory\model\SpaceAuthKeyConfigSndInfoFactory;



return array(
    // Space authentication key requisition request sending information services
    // ******************************************************************************

    'people_space_auth_key_requisition_request_snd_info_factory' => [
        'source' => SpaceAuthKeyConfigSndInfoFactory::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_config'],
            ['type' => 'config', 'value' => 'people/space_auth_key/requisition/request/snd_info_factory/config']
        ],
        'option' => [
            'shared' => true
        ]
    ]
);