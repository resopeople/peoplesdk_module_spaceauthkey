<?php

use people_sdk\space_auth_key\requisition\request\info\factory\model\SpaceAuthKeyConfigSndInfoFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'requisition' => [
            'request' => [
                'snd_info_factory' => [
                    'config' => [
                        'snd_info_factory' => [
                            ['snd_info_factory' => 'people_space_auth_key_requisition_request_snd_info_factory']
                        ]
                    ]
                ]
            ]
        ],

        'space_auth_key' => [
            'requisition' => [
                'request' => [
                    // Space authentication key requisition request sending information factory
                    'snd_info_factory' => [
                        /**
                         * Configuration array format:
                         * @see SpaceAuthKeyConfigSndInfoFactory configuration format.
                         */
                        'config' => [
                            'space_auth_key_support_type' => 'header',
                            'space_auth_key_current_include_config_key' => 'people_space_auth_key_current_include'
                        ]
                    ]
                ]
            ]
        ]
    ]
);