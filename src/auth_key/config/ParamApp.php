<?php

use people_sdk\space_auth_key\auth_key\model\SpaceAuthKeyEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'space_auth_key' => [
            // Space authentication key entity factory
            'factory' => [
                /**
                 * Configuration array format:
                 * @see SpaceAuthKeyEntityFactory configuration format.
                 */
                'config' => [
                    'select_entity_require' => true,
                    'select_entity_create_require' => true,
                    'select_entity_collection_set_require' => true
                ]
            ]
        ]
    ]
);