<?php

use people_sdk\module_space_auth_key\auth_key\boot\AuthKeyBootstrap;



return array(
    'people_space_auth_key_auth_key_bootstrap' => [
        'call' => [
            'class_path_pattern' => AuthKeyBootstrap::class . ':boot'
        ]
    ]
);