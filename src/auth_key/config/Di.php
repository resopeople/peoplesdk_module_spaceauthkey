<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\space_auth_key\auth_key\model\SpaceAuthKeyEntityCollection;
use people_sdk\space_auth_key\auth_key\model\SpaceAuthKeyEntityFactory;
use people_sdk\space_auth_key\auth_key\model\repository\SpaceAuthKeyEntitySimpleRepository;
use people_sdk\space_auth_key\auth_key\model\repository\SpaceAuthKeyEntityMultiRepository;
use people_sdk\space_auth_key\auth_key\model\repository\SpaceAuthKeyEntityMultiCollectionRepository;



return array(
    // Space authentication key entity services
    // ******************************************************************************

    'people_space_auth_key_entity_collection' => [
        'source' => SpaceAuthKeyEntityCollection::class
    ],

    'people_space_auth_key_entity_factory_collection' => [
        'source' => SpaceAuthKeyEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_space_auth_key_entity_factory' => [
        'source' => SpaceAuthKeyEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_space_auth_key_entity_factory_collection']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_space_auth_key_entity_simple_repository' => [
        'source' => SpaceAuthKeyEntitySimpleRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_space_auth_key_entity_multi_repository' => [
        'source' => SpaceAuthKeyEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_space_auth_key_entity_multi_collection_repository' => [
        'source' => SpaceAuthKeyEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_space_auth_key_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_space_auth_key_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    SpaceAuthKeyEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_space_auth_key_entity_collection']
    ]
);