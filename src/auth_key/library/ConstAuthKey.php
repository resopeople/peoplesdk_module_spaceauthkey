<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_space_auth_key\auth_key\library;



class ConstAuthKey
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration module
    const MODULE_KEY = 'people_space_auth_key_auth_key';



}