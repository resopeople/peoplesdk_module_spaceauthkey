<?php
/**
 * Description :
 * This class allows to define authentication key module bootstrap class.
 * Authentication key module bootstrap allows to boot authentication key module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_space_auth_key\auth_key\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;
use people_sdk\space_auth_key\auth_key\model\SpaceAuthKeyEntityFactory;
use people_sdk\module_space_auth_key\auth_key\library\ConstAuthKey;



class AuthKeyBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application configuration instance.
     * @var ConfigInterface
     */
    protected $objAppConfig;



    /**
     * DI: Space authentication key entity factory instance.
     * @var SpaceAuthKeyEntityFactory
     */
    protected $objSpaceAuthKeyEntityFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objAppConfig
     * @param SpaceAuthKeyEntityFactory $objSpaceAuthKeyEntityFactory
     */
    public function __construct(
        AppInterface $objApp,
        ConfigInterface $objAppConfig,
        SpaceAuthKeyEntityFactory $objSpaceAuthKeyEntityFactory
    )
    {
        // Init properties
        $this->objAppConfig = $objAppConfig;
        $this->objSpaceAuthKeyEntityFactory = $objSpaceAuthKeyEntityFactory;

        // Call parent constructor
        parent::__construct($objApp, ConstAuthKey::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Set space authentication key entity factory configuration, if required
        if(
            is_array($tabConfig = $this->objAppConfig->getValue(
                ToolBoxConfig::getStrPathKeyFromList('people', 'space_auth_key', 'factory', 'config')
            )) &&
            (count($tabConfig) > 0)
        )
        {
            $tabConfig = array_merge($this->objSpaceAuthKeyEntityFactory->getTabConfig(), $tabConfig);
            $this->objSpaceAuthKeyEntityFactory->setTabConfig($tabConfig);
        };
    }



}